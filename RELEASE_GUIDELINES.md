# Release Guidelines for the LinkAhead Project.

Version: 0.1
Modified: 2020-01-28
Author: Timm Fitschen

1. We roughly follow a Rolling-Release Strategy

   All features, experimental or well-hung, go into a release, as long all
   release requirements are met.

   Rationale:

   Rolling releases require less planning overhead than point release
   strategies. The planning is basically done during the planning of the
   development itself. Everything which is ready, is ready to be released.

   Rolling releases are said to be more likely to contain bugs, as they do
   sometimes include experimental software. However, unless we have a detailed
   auditing and selection process (which we don't have), the best way to test
   new pieces of our software for its stability in the wild, is shipping it :)

2. Releases are to made on a regular basis. The release branch is the `main`
   branch of each repository. New code is committed to the `main` if
   all release requirements are met. The release requirements are to be defined
   separately for earch repository.

   A RELEASE_GUIDELINES.md in each repository may declare more requirements,
   which must be met. It should furthermore reference this document.

   The release guidelines should include:
     * All Tests passing
     * FEATURES.md is up-to-date with a public API being declared.
     * CHANGELOG.md is up-to-date.
     * DEPENDENCIES.md is up-to-date and checked for plausibility.

   The CHANGELOG.md file contains a changelog following the principles of
   [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

   The DEPENDENCIES.md file contains a list of dependencies, with versions.
   This file is perhaps generated or enriched by the build tools (mvn,
   setup_tools).

   The FEATURES.md contains a list of features (possibly with versions of the
   features) with a note whether they are experimental and a reference to the
   document with the declaration of the public API of the features.

3. In order to make it easier to fix a release while there is still heavy
   development done in the `dev` branch, it is recommended, but not strictly
   required, to branch out a release branch from `dev` and do all necessary
   adjustments there, before merging the release branch into `main`.

4. The release schedule (when to publish a new release) depends on the velocity
   of the development. New releases for untouched branches are not desired and
   bulk releases of thousand new features should be prevented in the future.

   Instead, a repository with active development should be released as soon as
   a new feature is ready. There is nothing wrong about a new patch release
   with just two bugs fixed.

5. To maintain a minimal stability guarantee in the realeases, we mark features
   as `EXPERIMENTAL` when they are very new, poorly tested or hardly
   documented. If possible, we should make experimental features optional and
   turn them off by default in the main branch.

   The experimental state of a feature is to be declared in the FEATURES.md

6. Versioning of the Releases should follow the principles of semantic
   versioning ([SemVer 2.0.0](https://semver.org/)). That implies that software
   can only have non-zero major version numbers, if there is a declared public
   API which is normative for the implementation.

   If experimental features are included into the release, they must be
   declared as non-normative extensions to the API and hence the experimental
   state propagates to all software which depends on the experimental feature.

   This makes it possible to release experimental software without having to
   bump the major version with every release (and thereby making nonsense of
   semantic versioning).

7. Dependencies of releases must be declared in a DEPENDENCIES.md file, which
   includes the exact version of the dependency. If a piece of software in
   a release depends on experimental features of a dependency, the feature must
   itself be marked as experimental.

8. Recommendations for versioned features.

   When software is composed entirely or partly of rather independent modules
   (server jobs, server-side scripts, and especially WebUI modules) it makes
   sense to also version particular features of a software in one repository.

   This will make the CHANGELOG.md more informative because it is easier for
   a maintainer of a dependent features to spot API changes which are relevant
   for the dependent feature.

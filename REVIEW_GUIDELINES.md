# Summary

Reviewing a merge request can be a complex and consuming task. These guidelines
should help both the authors of the code changes and the reviewers to make it a
satisfying and effective activity.

# Review Guidelines for Authors

0. Use the merge request templates which should be available in our
   repositories. They contain sections which have to be completed by the
   author, for example:
   * Summary - A short description of the merge request. What is the
     new/changed behavior? Which bug has been fixed? Are there related Issues?
   * Focus - Point the reviewer to the core of the code change. Where should
     they start reading? What should they focus on (e.g. security, performance,
     maintainability, user-friendliness, compliance with the specs, finding
     more corner cases, concrete questions)?
   * Test Environment - A sufficient description how the reviewer can set up
     a test environment for manual testing.
   * A check list of things the author should do *before* the reviewer starts
     their work.

1. Prepare your code before requesting the review. Make sure to have
   * Up-to-date documentation (wrong documentation is worse than no
     documentation at all).
   * Annotate your code via Gitlab comments and point out the most relevant
     changes and open questions, reference other relevant pieces of the code,
     changes in the specs specs, user-stories, and whatever makes it easier to
     understand why you did something the way you did it. These annotation are
     not part of the code itself, because they should explain the changes
     rather than the code and guide the reviewer through your changes.

2. Expect to change your code: If the reviewer requests you of doing something
   in a different way: lean towards their proposal - just do it unless you have
   arguments *against* their proposal.

# Review Guidelines for Reviewers

0. Don't review for longer than 1h and up to 300 lines in one session. Split
   your review in smaller chunks or review in several iterations.
1. Build and run the code before reading. Test the behavior. If you cannot set
   up the test environment stop the review and speak to the author.
2. Get yourself an overview: Which functions/classes are relevant for
   understanding the MR and the new behavior?
3. Try using the Side-by-side diff view of Gitlab. Much better for
   understanding the new state of the code. (Click the gear sign, right upper
   corner in the diff view).
4. Work through the open comment threads and close them if possible. The should
   guide you through the review, explain what the author was trying to achieve,
   or point out open questions.
5. Read the changed code, try to understand every line. It's worth it. Create
   Gitlab comments as you proceed if questions arise or if you want to propose
   an alternative. Also read code that has not been changed if it is relevant
   for the new code.
6. Also check: Are the code documentation/comments up-to-date? Have there been
   behavioral changes without amending the documentation?
7. If you have a hard time understanding some piece of code - get back to the
   author with your questions soon. Don't try to figure it out too long.

# Note on squash merges

We disabled Gitlab's option to squash all commits of the source branch into one
single merge commit in the target branch and strongly discourage the usage of
such squash merges in general. While possibly serving to keep the history of the
target branch clean, they hide information about the content and contributors of
individual commits. Also, they pose the risk of merge conflicts due to
incompatible commit histories of different branches. We do not, however, forbid
squash merges altogether, so if both, author(s) and reviewer agree, go ahead and
use `git rebase -i` to squash your commits locally.

# Reverting a botched merge

In case something is overlooked during the review or errors only become apparent
after a branch has been merged, it may become necessary to revert the changes in
the target branch, restoring its state before the merge. In order to do this
cleanly, and to check whether a reversion of the merge really fixes the error(s)
it introduced, do the following.

1. Go to the overview of the merged MR on Gitlab and click on "revert". Choose
   the target branch (usually `dev`) and select "Start a new merge request with
   these changes". Click revert. A new merge requests for a branch
   `revert-<commit-hash>` opens in which the old changes are reverted. You may
   also create a new branch and revert the changes locally using `git revert` if
   you know what you are doing.
2. Wait for any pipelines to succeed on the `revert-...` branch and/or test
   manually whether all newly introduced problems have indeed been fixed by the
   reversion. Merge the `revert-...` branch only after you've made certain that
   it will leave the target branch in a working state.
3. If you want to fix the original changes, you have to create a new feature
   branch in which you revert the merge of the `revert-...` branch using `git
   revert --no-commit --edit <hash-of-merge-commit>` locally. You may then
   continue developing and fixing the original problem. Note that a new code
   review is necessary before this new feature branch may be merged into the
   target branch.

Please note that this is a kind of last resort and please review merge requests
carefully so that we do not have to revert anything at all.

# References

* https://smartbear.com/learn/code-review/best-practices-for-peer-code-review/
* https://www.perforce.com/blog/qac/9-best-practices-for-code-review
* https://dev.to/codemouse92/10-principles-of-a-good-code-review-2eg
* https://phauer.com/2018/code-review-guidelines/

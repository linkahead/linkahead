# Welcome

LinkAhead is a software toolbox for data management. Its focus lies on managing
data in dynamic environments of research and development where changes are too
frequent or projects are too small to bear the great cost of implementing a
traditional specialized but inflexible data management system. The research
data management system (RDMS) LinkAhead uses a semantic data model that allows to
grow and change in those dynamic environments!

**Note:** LinkAhead was previously also known as CaosDB. See https://www.indiscale.com/news/
for more information.

You can find an [online demo](https://demo.indiscale.com) of LinkAhead at [https://demo.indiscale.com](https://demo.indiscale.com).  The collected
documentation is hosted at [https://docs.indiscale.com](https://docs.indiscale.com).

If you want to get your feet wet yourself, you should take a look at [our
recommended way to start/use the LinkAhead server](https://gitlab.com/linkahead/linkahead-control)
or install the [Debian package](https://indiscale.com/download).

This is the LinkAhead meta repository. It contains general documents such as our [Code of Conduct](CODE_OF_CONDUCT.md)
and general guidelines which apply to all LinkAhead subprojects. The source code
of the various components of LinkAhead resides in separate repositories (see below).

## Useful links ##

- Documentation: https://docs.indiscale.com
- Chat: [#linkahead:matrix.org](https://matrix.to/#/!unwwlTfOznjEnMMXxf:matrix.org)

## Documents ##

- [Code of Conduct](CODE_OF_CONDUCT.md) We want everyone to feel safe and respected in the LinkAhead community.  Our
  code of conduct is an effort to achieve this goal.
- [Our contributors](HUMANS.md) These people contributed to the LinkAhead project.
- [Release Guidelines](RELEASE_GUIDELINES.md) What to do when releasing a new version of a subproject.  The LinkAhead
  subprojects often have more specific additional guidelines.
- [Review Guidelines](REVIEW_GUIDELINES.md) for satisfying and effective code reviews.

## LinkAhead subprojects ##

### Good starting points ###

- [Python client library](https://gitlab.com/linkahead/linkahead-pylib) contains the Python client. Most users want to start here. You can for
  example access the demo server with the client.
  - [Advanced Python tools](https://gitlab.com/linkahead/linkahead-advanced-user-tools) that go beyond a simple use of the Python client. Especially, this
    includes the crawler for automated data integration.
- [linkahead-server](https://gitlab.com/linkahead/linkahead-server) contains the java sources of the server software. This is was you need to run
  your own LinkAhead instance.
- [linkahead-webui](https://gitlab.com/linkahead/linkahead-webui) contains the web client (is typically installed with linkahead-server and is a
  submodule of it).

### More APIs and client libraries ###

- [LinkAhead Protobuf API](https://gitlab.com/linkahead/linkahead-proto) the protobuf files which make up the gRPC API of LinkAhead.
- [C++ library](https://gitlab.com/linkahead/linkahead-cpplib) is the C++ core library for accessing the gRPC API.
- [Julia client library](https://gitlab.com/linkahead/linkahead-julialib) for interaction with LinkAhead through Julia.  Uses the C++ library.
- [Octave client library](https://gitlab.com/linkahead/linkahead-octavelib) for interaction with LinkAhead through Octave/Matlab.  Uses the C++
  library.

### Miscellaneous ###

- [Docker](https://gitlab.com/linkahead/linkahead-docker) for running LinkAhead and other components in a Docker environment.
- [Crawler](https://gitlab.com/linkahead/linkahead-crawler) is the new crawler system for automated data integration, but still WIP (as of
  2022-09).
- [linkahead-mysqlbackend](https://gitlab.com/linkahead/linkahead-mysqlbackend) contains sources for setting up and interacting with the MySQL/MariaDB
  backend. You need it for running the server.
- [Python integration tests](https://gitlab.com/linkahead/linkahead-pyinttest) the integration tests for the system with the highest coverage.
- [Julia integration tests](https://gitlab.com/linkahead/linkahead-juliainttest) integration tests for Julia.

# Maintainers #

* Timm Fitschen (Lead)
* Daniel Hornung
* Alexander Schlemmer
* Henrik tom Wörden

# License #

Generally, the LinkAhead project and all files in the official repositories are licensed under the GNU
AGPLv3 [License](LICENSE.md).  However, always check the subprojects' `LICENSE.md` files for the
license which applies in the particular case.
